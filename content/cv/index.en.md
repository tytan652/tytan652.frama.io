---
title: CV
license: CC BY-NC-SA 4.0
lastmod: 2024-01-29
---

# Random free/libre software developer

This bloc of text should be a catchphrase, please do not expect it to be catching.

## Formations

- BTS SN IR obtain in 2019

- BAC STI2D SIN obtain in 2017

## Skill

- Inability to self-evaluate on area of expertise (e.g. "C/C++") or a language on a simplified scale (e.g. France Travail propose to chose between "Beginner", "Intermediate", "Advanced") 
- Vision incompatible with what is commonly referred as proprietary software
- Preference to chose licenses from the GPL family (`GPL-2.0-or-later`, `LGPL-2.1-or-later`, `AGPL-3.0-or-later`), so "contaminating"
- C Application C with GNOME stack (GTK + Libawaita)
  - Creation of [Door Knocker](../projects/#door-knocker)
- C/C++ Application with Qt
  - [Contribution to OBS Studio](../projects/#obs-studio)
- And I may have forgotten some things that could go here…

## Language

- French (mother tongue)
- English

