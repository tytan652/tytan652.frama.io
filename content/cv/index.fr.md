---
title: CV
license: CC BY-NC-SA 4.0
lastmod: 2024-01-29
---

# Développeur de logiciel libre lambda

Ce bloc de texte est censé est une accroche, s'il vous plaît ne vous attendez à ce soit accrochant.

## Formations

- BTS SN IR obtenu en 2019

- BAC STI2D SIN obtenu en 2017

## Compétences

- Incapacité à s'auto-évaluer sur des domaine d'expertise (e.g. "C/C++") ou une langue sur une échelle simplifié (e.g. France Travail propose de choisir entre "Débutant", "Intermédiaire", "Avancé") 
- Vision incompatible avec ce qui est communément appelé logiciel propriétaire
- Préférence à choisir des licences de la famille GPL (`GPL-2.0-or-later`, `LGPL-2.1-or-later`, `AGPL-3.0-or-later`), donc "contaminante"
- Application C avec la pile GNOME (GTK + Libawaita)
  - Création de [Door Knocker](../projects/#door-knocker)
- Application C/C++ avec Qt
  - [Contribution à OBS Studio](../projects/#obs-studio)
- Et j'ai peut-être oublié des éléments qui pourraient aller ici… 

## Langues

- Français (langue maternel)
- Anglais

