---
title: "About me"
license: CC BY-NC-SA 4.0
lastmod: 2023-01-07
---

I call myself a FLOSS developer with a preference towards non-web software development.

## Status

Since early December 2022, I have registered as an independent free/libre software (FLOSS/FOSS) developer in France with the french self-employment status.

This status does not prevent me to have a primary job, this status will be considered as complementary.

### Why this status?

1. I'm unemployed since my graduation (BTS SN IR), so I thought I should try something even if it's a stupid choice.
    - Why stupid? I don't think I have what it takes to find clients and stuff.
2. In France, receiving donations without being a company or an association seems maybe weirdly complex so this status removes that problem. I'd rather pay taxes on these donations than be in a potential grey area.

## What kind of projects am I working on?

Only free/libre software (FLOSS).

You can check [My projects](../projects) page for more detail.

## How do I finance myself?

- I don't earn a living and I'm lucky to be able to live with my family.
- Technically I should do projects on demand.

But most of my projects are my own initiative. If you find my work useful, it is also possible to make donations via the method listed on the [Donate](../donate) page.

## More about me

Let's cut to the chase, I hate to self-describe, so the following might be a bit messy/snappy.

- I live in France.
- I don't want to force myself to do something I don't want to do as a job.
  - I feel that forcing myself will hurt me and others more than helping.
  - Getting a paycheck while suffering is not worth it to me.
- I have no desire to get a driver's license.
- I am potentially lost in this world.

