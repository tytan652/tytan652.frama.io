---
title: "À propos de moi"
license: CC BY-NC-SA 4.0
lastmod: 2023-01-07
---

Je me présente comme un développeur de logiciel libre avec une préférence envers le développement logiciel non-web.

## Status

Depuis début décembre 2022, je me suis déclaré développeur indépendant de logiciels libres (FLOSS/FOSS) en France avec le statut d'auto-entrepreneur.

Ce statut ne m'empêche pas d'avoir une activité principal, ce statut sera considéré comme à titre complémentaire.

### Pourquoi ce statut ?

1. Je suis sans emploi depuis l'obtention de mon diplôme (BTS SN IR), donc je me suis dis qu'il fallait essayé quelque chose même si c'est un choix stupide.
    - Pourquoi stupide ? Je ne pense pas avoir ce qu'il faut pour trouver des clients et tout le touintouin.
2. En France, recevoir des dons sans être une stucture (entreprise ou association) semble peut-être bizarement complexe donc ce statut retire ce problème. Je prefère payer des taxes sur ces dons que être dans une potentielle zone grise.

## Sur quels types de projets je travaille ?

Uniquement du logiciel libre (FLOSS).

Vous pouvez ma page [Mes projets](../projects) pour plus de détail. 

## Comment je me finance ?

- Je ne gagne pas ma vie and je suis chanceux de pouvoir vivre avec ma famille.
- Techniquement je devrais faire des projets à la demande de client.

Mais la plupart de mes projets son à mon initiative. Si vous jugez mon travail utile, il est également possible de me faire des dons via les méthodes listées sur la page [Faire un don](../donate).

## Plus à propos de moi

Allons droit au but, je déteste m'auto-décrire, donc ce qui suit risque d'être un peu désordonnée/brève.

- Je vis en France.
- Je ne veux pas me forcer à faire quelque chose que je ne veux pas faire en tant qu'emploi.
  - Je sens que me forcer fera plus de mal à moi et aux autres qu'aider.
  - Obtenir un salaire en souffant, ça vaut pas le coup pour moi.
- Je n'ai aucune envie d'avoir le permis de conduire.
- Je suis potentiellement paumé dans ce monde.

