---
author: "tytan652"
title: "[OLD] The OBS Studio package on Arch Linux"
date: "2021-08-01"
description: "One of the worst unofficial package"
license: CC BY-NC-SA 4.0
---

***This is an old article, that I want to keep.***

This package had many issues, and still has some.

## Non-feature complete
This OBS Studio package share the common issue but it had/has more.

Note: This package is also Manjaro's one.

I will talk about them from the less understandable one to the most one.

### VST filter
There is a [feature request](https://bugs.archlinux.org/task/70128) on Arch Linux but I quote Jonathan Steel:
> One of my issues here is upstream does not include these things in their release. Arch will generally take what upstream releases and package that, which is what has happened here. Of course enabling build options and adding dependencies is a common thing to do to add additional features but here, it seems they want us to pull from git in order to obtain software that they do not include in their release. Call me confused with this decision.

> I'd like to see upstream provide a full release that includes these, or some evidence that obs has worked with the latest cef version without issues for some time before reconsidering switching to git.

I answered to those parts with this:
> I don't know if you know this but github source code tarball doesn't contain submodule, so github will never release a "full" source code tarball with submodules.

OBS Project can't do anything about the submodule issue so we can wait an eternity.

Another quote of Jonathan:
>Now, is it worth switching to git to be able to provide this plugin? Well, to satisfy the 2 users who have voted for this feature request I'm sitting on it for now.

I kindly anwered:
> I think everyone who wanted the VST plugin in OBS have just switched to an AUR package and didn't even think about coming here and ask for it or vote.

*He was also talking about votes in AUR.*

I was thinking "It is a joke right, seriously ?", how can you tell this knowing that there is AUR recipe who does the job and so users will never go to this [feature request](https://bugs.archlinux.org/task/70128) and vote.

I recall that Manjaro provide the same package, so more user are affected by this if they don't want Flatpak package. So this is not just "2 users".

**Update**: It seems they use the fact that github tarball are without submodules and also the fact that OBS Project doesn't provide an alternative as an excuse to never provide VST filter.

I don't want to talk about Snap here.

### FTL protocol
There is a [feature request](https://bugs.archlinux.org/task/55441).

There is a need to add an another package to the repo so it's stuck surely because *there is no vote*.

**Update**: Bull's eye, it was really because there is no vote.

### VLC sources

#### Before and during the [bug report](https://bugs.archlinux.org/task/70794)

VLC package is built with Lua 5.2, and OBS Studio need and is built with Lua JIT (≃ Lua 5.1). So when you added a VLC source to OBS, the latter will crash because of the difference of Lua version with VLC Lua library.

There was three solution:
- Ask for officialy build VLC only with Lua JIT.
- Disable VLC sources from OBS Studio.
- Disable Lua scriting from OBS or VLC, not in the [bug report](https://bugs.archlinux.org/task/70794) because it cause more hinder and it's overkill.

Meanwhile I created vlc-luajit to solve the issue.

#### After the [bug report](https://bugs.archlinux.org/task/70794) closure

The solution was chosen making the package more feature incomplete. Because of the nature of Arch Linux, a rolling release, making VLC use the latest as possible Lua is normal.

### Browser dock & source
There was a [feature request](https://bugs.archlinux.org/task/66008) but the fact that obs-browser does not support the latest version\* of the needed dependency was enough. Which is fair.

\**Even if it does it's not supported by OBS Project.*

## Not well maintained
Once OBS Studio 27 was out, the recipe to make the package was a mess and I made a [feature request](https://bugs.archlinux.org/task/71156) for this.

The biggest was the fact that since PipeWire and the Xcomposite library were not installed as required many people lost the option to make screen capture because these two package were not both installed.

## One good point
They patched the package to be able to work with the latest version of Python
