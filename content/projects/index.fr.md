---
title: "Mes projets"
license: CC BY-NC-SA 4.0
lastmod: 2024-01-30
---

La majorité des liens mèneront vers des pages en anglais.

# Creations

## [Door Knocker](https://codeberg.org/tytan652/door-knocker)

Door Knocker est un simple outil pour vérifier la disponibilité de [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal) écrit en C avec GTK4 et Libadwaita.

[![Télécharger Door Knocker sur Flathub](https://flathub.org/assets/badges/flathub-badge-en.png)](https://flathub.org/apps/xyz.tytanium.DoorKnocker)

# Contributions

## OBS Studio

La plupart de mes projets sont liés à OBS Studio.

### Service Overhaul

*À besoin d'un re-design pour une transition plus douce dans le code et l'UI/UX.*

Refonte de la gestion des services dans OBS Studio et possibilité d'ajouter des services par le biais de plugins.

[Lien vers la RFC](https://github.com/obsproject/rfcs/pull/39) qui dépend sur une autre [RFC](https://github.com/obsproject/rfcs/pull/45).

### Passer au modèle MVC (En pause)

Le code actuel est presque une chimère de modèles architecturaux logiciels, l'application du modèle MVC permettra de démêler le code et de le rendre plus clair.

[Lien vers la RFC](https://github.com/obsproject/rfcs/pull/53).

### Changer de système de dock (En pause)

[Lien vers la RFC](https://github.com/obsproject/rfcs/pull/47) mais il base sur ADS, lequel pour le moment est non-fonctionnel sous Wayland.

### Plugin PeerTube

*Requiert [Service Overhaul](#service-overhaul).*

Créer un plugin qui ajoute une integration PeerTube à OBS Studio.

### Autre PRs

Vous pouvez trouver mes autre travaille à propos d'OBS Studio [ici](https://github.com/obsproject/obs-studio/pulls?q=is%3Apr+author%3Atytan652).

## [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal)

Permet de partager des stream PipeWire à travers des portals pour divers cas d'utilisations.

[Lien vers la discussion GitHub](https://github.com/flatpak/xdg-desktop-portal/discussions/1141).

## [Arch User Repository (AUR)](https://wiki.archlinux.org/title/Arch_User_Repository_(Fran%C3%A7ais))

Je maintiens 50+ paquets avec la majorité d'entre eux étant des plugins pour OBS Studio.

