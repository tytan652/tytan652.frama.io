---
title: "My projects"
license: CC BY-NC-SA 4.0
lastmod: 2024-01-30
---

# Creation

## [Door Knocker](https://codeberg.org/tytan652/door-knocker)

Door Knocker is a simple tool to check the availability of [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal) written in C with GTK4 and Libadwaita.

[![Download Door Knocker on Flathub](https://flathub.org/assets/badges/flathub-badge-en.png)](https://flathub.org/apps/xyz.tytanium.DoorKnocker)

# Contribution

## OBS Studio

Most of my project are related to OBS Studio.

### Service Overhaul

*Needs a redesign for a smoother transition code-wise and UI/UX-wise.*

Overhaul how services is managed in OBS Studio and allow adding services through plugins.

[Link to the RFC](https://github.com/obsproject/rfcs/pull/39) which depends on another [RFC](https://github.com/obsproject/rfcs/pull/45).

### Move to MVC pattern (Paused)

The actual code is almost a chimera of software architectural patterns, applying the MVC pattern will allow to untangle the code and make it more clear.

[Link to the RFC](https://github.com/obsproject/rfcs/pull/53).

### Switch docking system (Paused)

[Link to the RFC](https://github.com/obsproject/rfcs/pull/47) but it is based on ADS, which for now is non-functional under Wayland.

### PeerTube plugin

*Requires [Service Overhaul](#service-overhaul).*

Create a plugin that adds a PeerTube integration to OBS Studio.

### Other PRs

You can find my other works about OBS Studio [here](https://github.com/obsproject/obs-studio/pulls?q=is%3Apr+author%3Atytan652).

## [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal)

### App to App Media Sharing

Enable sharing PipeWire stream through portals for multiple usecases.

[Link to the GitHub Discussion](https://github.com/flatpak/xdg-desktop-portal/discussions/1141).

## [Arch User Repository (AUR)](https://wiki.archlinux.org/title/Arch_User_Repository)

I maintain 50+ packages with most of them being OBS Studio plugins.

