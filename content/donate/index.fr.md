---
title: Faire un don
license: CC BY-NC-SA 4.0
lastmod: 2024-01-29
---

En France, pour recevoir des donations en tant que particulier il est requis que ce dernier soi au moins une micro-entreprise, ce statut peut-être garder en parallèle d'une activité principale.

Dans une micro-entreprise, les bénéfice sont séparés en divers catégorie, 3 pour mon "secteur d'activité":
1. Achat/revente de marchandises (BIC) 
2. Prestations de services commerciales et artisanales (BIC)
3. Autres prestations de services (BNC)

En fonction de comment le⋅a mécénat/donation est faite, cela entre dans la seconde ou la troisième catégorie.
Si le mécène reçoit rien en échange, c'est la troisième catégorie. Autrement c'est la deuxième.

Donc pour éviter des problème de comptabilité, les plateformes qui suivent sont mis en place/utiliser de manière à ce que les dons d'une plateforme entre dans une seule catégorie.

## [Liberapay](https://fr.liberapay.com)

*Liberapay est disponible en français*

[![Faire un don récurrent via Liberapay](https://liberapay.com/assets/widgets/donate.svg)](https://fr.liberapay.com/tytan652)

Cette plateforme rentre dans la troisième catégorie.

## [Ko-fi](https://ko-fi.com)

*Ko-fi n'est pas disponible en français*

[![Faire un don via Ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/tytan652)

Pour garder toutes les donations de cette plateforme dans la troisième catégorie, je n'utilise pas la fonctionnalité "Thank you".

## [GitHub Sponsors](https://github.com/sponsors)

*GitHub Sponsors ne semble pas être disponible en français*

Principalement mit en place pour les personnes qui préfèrent vraiment cette plateforme aux précédente.

[tytan652's sponsor profile](https://github.com/sponsors/tytan652)

Aucun tiers ou récompense pour garder les donations dans la troisième catégorie.

