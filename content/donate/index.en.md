---
title: Donate
license: CC BY-NC-SA 4.0
lastmod: 2024-01-29
---

In France, to receive donation as an individual requires the individual to be at least a self-employment, this status can be kept in parallel of a primary activity.

In a french self-employment, profits are separated in various categories, 3 for my "branch of activity":
1. Purchase/sale of goods (industrial and commercial profits)
2. Commercial and craft services (industrial and commercial profits)
3. Other services (non-commercial profits)

Depending on how sponsorship/donation is made, this can be put in second or third category.
If the patron receives nothing in exchange, it's the third category. Otherwise it's the second.

So to avoid some accounting trouble, the following platforms will be setup/used in a way to make donations from the platform enter only one category.

## [Liberapay](https://liberapay.com)

[![Make a recurring donation via Liberapay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/tytan652)

The platforms fits in third category.

## [Ko-fi](https://ko-fi.com)

[![Make a donation via Ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/tytan652)

To keep all donations from this platform in the third category, I don't use the "Thank you" feature.

## [GitHub Sponsors](https://github.com/sponsors)

This is mainly setup for people really preferring this platform over the previous.

[tytan652's sponsor profile](https://github.com/sponsors/tytan652)

No tiers and rewards to keep donations in the third category.

